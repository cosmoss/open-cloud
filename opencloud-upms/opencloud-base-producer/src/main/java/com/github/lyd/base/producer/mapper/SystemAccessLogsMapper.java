package com.github.lyd.base.producer.mapper;

import com.github.lyd.base.client.entity.SystemAccessLogs;
import com.github.lyd.common.mapper.CrudMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface SystemAccessLogsMapper extends CrudMapper<SystemAccessLogs> {
}
